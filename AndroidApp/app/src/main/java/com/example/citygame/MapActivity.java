package com.example.citygame;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static android.os.Environment.getExternalStoragePublicDirectory;

public class MapActivity extends AppCompatActivity implements OnMapReadyCallback, InfoWindowAdapter, TaskLoadedCallback, OnInfoWindowClickListener {

//    private static final LatLng RATUSZ = new LatLng(54.34875, 18.652917);
//    private static final LatLng BAZYLIKA = new LatLng(54.349842, 18.653497);
//    private static final LatLng NEPTUN = new LatLng(54.348575, 18.653364);
//    private static final LatLng GRABÓWEK = new LatLng(54.52845, 18.493277);
    Button startTheGame;             //rozpoczęcie gry
//    private Marker mRatusz;
//    private Marker mBazylika;
//    private Marker mGrabowek;

    private List<Marker> markersList = new ArrayList<Marker>(); // do przechowywania punktów kontrolnych bez AKTUALNEJ POZYCJI
    ImageButton showAllMarkers;      //zoomowanie wszystkich markerów
    ImageButton takePic;             //robienie zdjęcia
    private Marker mNeptun;

    LatLngBounds.Builder builder;   //do otoczenia wszystkich markerów i ustawienia zoom'u na wszystkie markery
    CameraUpdate cameraUpdate;
    ImageButton goToCurrentPosition; //zoom do aktualnej pozycji
    String pathToFile;  //ścieżka do pliku
    LatLng latLng;  //aktualna pozycja

    boolean isRouted = false;

    ImageView picture;
    private Marker currentlyClickedMarker;
    private Marker markerCurrentPosition;

    private static final int REQUEST_LOCATION_PERMISSION = 1;


    LocationManager locationManager;
    LocationListener locationListener;
    private Circle circle;      //kolo wokół aktualnej pozycji
    private Polyline currentPolyline;  // wyznaczenia trasy

    private GoogleMap mMap;

    private Scenario scenario;

    /**
     * przejscie do obecnej lokalizacji
     */


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        scenario = (Scenario)getIntent().getSerializableExtra("scenario");

        picture = findViewById(R.id.picture);
        goToCurrentPosition = findViewById(R.id.goToCurrentPosition);
        takePic = findViewById(R.id.takePic);
        showAllMarkers = findViewById(R.id.showAllMarkers);
        startTheGame = findViewById(R.id.startTheGame);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        if (Build.VERSION.SDK_INT >= 23) {
            requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2);
        }

        showAllMarkers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSetUpMap();
            }
        });

        takePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dispatchPictureTakeAction();
            }
        });

        goToCurrentPosition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMap.setMaxZoomPreference(20);
                //  mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 21.0f));
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 20));
            }
        });

        startTheGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openGameActivity();

            }
        });

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]
                            {Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_LOCATION_PERMISSION);
        }

        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                double latitude = location.getLatitude();
                double longitude = location.getLongitude();
                //get the location name from latitude and longitude
                Geocoder geocoder = new Geocoder(getApplicationContext());
                try {
                    List<Address> addresses =
                            geocoder.getFromLocation(latitude, longitude, 1);
                    String result = addresses.get(0).getLocality() + ":";
                    result += addresses.get(0).getCountryName();
                    latLng = new LatLng(latitude, longitude);

                    if (markerCurrentPosition != null && circle != null) {
                        goToCurrentPosition.setVisibility(View.VISIBLE);
                        markerCurrentPosition.remove();
                        updateMarkerWithCircle(latLng, result, latitude, longitude);

                    } else {
                        drawMarkerWithCircle(latLng, result);
                        double target_lat = getIntent().getDoubleExtra("lat", 1.0);
                        double target_lon = getIntent().getDoubleExtra("lon", 1.0);
                        latLng = new LatLng(target_lat, target_lon);
                        Log.d("dupa", "lat = " + target_lat + " lon = " + target_lon);
                        mMap.addMarker(new MarkerOptions().position(latLng).title(result));

                        //marker = mMap.addMarker(new MarkerOptions().position(latLng).title(result));
                        //mMap.setMaxZoomPreference(20);
                        //mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 21.0f));
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            private void drawMarkerWithCircle(LatLng position, String result) {
                double radiusInMeters = 100.0;
                int strokeColor = 0xffff0000; //red outline
                int shadeColor = 0x44ff0000; //opaque red fill

                CircleOptions circleOptions = new CircleOptions().center(position).radius(radiusInMeters).fillColor(shadeColor).strokeColor(strokeColor).strokeWidth(8);
                circle = mMap.addCircle(circleOptions);

                MarkerOptions markerOptions = new MarkerOptions().position(position).title(result);
                markerCurrentPosition = mMap.addMarker(markerOptions);

            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        };
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);

    }

    private void openGameActivity() {
        LatLng position = new LatLng(currentlyClickedMarker.getPosition().latitude, currentlyClickedMarker.getPosition().longitude);
        Intent gameActivity = new Intent(this, GameActivity.class);
        gameActivity.putExtra("lat", position.latitude);
        gameActivity.putExtra("long", position.longitude);
        gameActivity.putExtra("scenario", scenario);
        startActivity(gameActivity);

    }

    private String getUrl(LatLng origin, LatLng dest, String directionMode) {
        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;
        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;
        // Mode
        String mode = "mode=" + directionMode;
        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + mode;
        // Output format
        String output = "json";
        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters + "&key=" + getString(R.string.map_key);
        return url;
    }

    private void mSetUpMap() {

        if (markersList.size() < 1)
            return;
        /**create for loop for get the latLngbuilder from the marker list*/
        builder = new LatLngBounds.Builder();
        for (Marker m : markersList) {
            builder.include(m.getPosition());
        }
        /**initialize the padding for map boundary*/
        int padding = 50;

        /**create the bounds from latlngBuilder to set into map camera*/
        LatLngBounds bounds = builder.build();

        /**create the camera with bounds and padding to set into map*/
        cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, padding);

        /**call the map call back to know map is loaded or not*/
        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                /**set animated zoom camera into map*/
                mMap.animateCamera(cameraUpdate);

            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                Bitmap bitmap = BitmapFactory.decodeFile(pathToFile);
                picture.setImageBitmap(bitmap);
            }
        }
    }

    private void dispatchPictureTakeAction() {
        Intent takePic = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);          //przechwycenie obrazu
        if (takePic.resolveActivity(getPackageManager()) != null) {             //czy istnieje aplikacja, ktora to zrobi
            File photoFile = null;                                              //miejsce zapisu fotek
            photoFile = createPhotoFile();

            if (photoFile != null) {
                pathToFile = photoFile.getAbsolutePath();
                Uri photoURI = FileProvider.getUriForFile(MapActivity.this, "com.example.citygame.fileprovider", photoFile);
                takePic.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePic, 1);
            }

        }
    }

    private File createPhotoFile() {                                // zapisywanie zdjec wedlug daty
        String name = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File storageDir = getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);        //otrzymanie dostepu przez wszystkie apki do fotek
        File image = null;
        try {
            image = File.createTempFile(name, ".jpg", storageDir);
        } catch (IOException e) {
            Log.d("mylog", "Excep : " + e.toString());
        }
        return image;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

        /**clear the map before redraw to them*/
        mMap.clear();

        mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        // Add markers to Map
        // addMarkersToMap();
        setMarkers();
        mSetUpMap();            /**Show all map markers in a bound*/

        mMap.setInfoWindowAdapter(this);
        mMap.setOnInfoWindowClickListener(this);
        // checkdistance();

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {

                for (Marker whichMarker : markersList) {

                    if (marker.equals(whichMarker)) {
                        Log.w("Click", "test onMarkerClick: " + whichMarker);
                        String url = getUrl(markerCurrentPosition.getPosition(), whichMarker.getPosition(), "walking");
                        new FetchURL(MapActivity.this).execute(url, "walking");
                        currentlyClickedMarker = marker;
                        isRouted = true;

                        return true;
                    }

                }
                return false;
            }
        });
    }

//    private void mSetUpMarkers() {
//        markersList.add(mRatusz);
//        markersList.add(mBazylika);
//        markersList.add(mNeptun);
//        markersList.add(mGrabowek);
//    }

//    private void checkdistance() {
//
//        LatLng markerLatLng = GRABÓWEK;
//        Location markerLocation = new Location("");
//        markerLocation.setLatitude(markerLatLng.latitude);
//        markerLocation.setLongitude(markerLatLng.longitude);
//
//        LatLng currentLatLng = markerLatLng;
//        Location currentLocation = new Location("");
//        markerLocation.setLatitude(currentLatLng.latitude);
//        markerLocation.setLongitude(currentLatLng.longitude);
//
//        if (currentLocation.distanceTo(markerLocation) < 100) {
//            System.out.println("Jestem w pobliżu");
//        }
//
//    }

//    private void addMarkersToMap() {
//
//        // Uses a colored icon.
//        mRatusz = mMap.addMarker(new MarkerOptions()
//                .position(RATUSZ)
//                .title("Warszawa")
//                .snippet("Stolica")
//                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));
//
//        // Uses a custom icon with the info window popping out of the center of the icon.
//        mBazylika = mMap.addMarker(new MarkerOptions()
//                .position(BAZYLIKA)
//                .title("Warka")
//                .snippet("Rzeka")
//                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE))
//                .infoWindowAnchor(0.5f, 0.5f));
//
//        // Creates a draggable marker. Long press to drag.
//        mNeptun = mMap.addMarker(new MarkerOptions()
//                .position(NEPTUN)
//                .title("Sochaczew")
//                .snippet("Zamek")
//                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));
//
//        // Creates a draggable marker. Long press to drag.
//        mGrabowek = mMap.addMarker(new MarkerOptions()
//                .position(GRABÓWEK)
//                .title("Grabówek")
//                .snippet("Domek")
//                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));
//
//    }

    @Override
    protected void onStop() {
        super.onStop();
        locationManager.removeUpdates(locationListener);
    }

    @Override
    public View getInfoWindow(Marker marker) {
        View view = getLayoutInflater().inflate(R.layout.info_window, null);
        render(marker, view);

        return view;
    }

    @Override
    public View getInfoContents(Marker marker) {
        return null;
    }

    private void render(Marker marker, View view) {
        int badge;
        // Use the equals() method on a Marker to check for equals.  Do not use ==.
        if (marker.equals(marker)) {
        badge = R.drawable.ic_action_location;
        } else {
            // Passing 0 to setImageResource will clear the image view.

            badge = 0;
        }
        ((ImageView) view.findViewById(R.id.badge)).setImageResource(badge);

        String title = marker.getTitle();
        TextView titleUi = ((TextView) view.findViewById(R.id.title));
        if (title != null) {
            // Spannable string allows us to edit the formatting of the text.
            SpannableString titleText = new SpannableString(title);
            titleText.setSpan(new ForegroundColorSpan(Color.RED), 0, titleText.length(), 0);
            titleUi.setText(titleText);
        } else {
            titleUi.setText("");
        }

        String snippet = marker.getSnippet();
        TextView snippetUi = ((TextView) view.findViewById(R.id.snippet));
        if (snippet != null && snippet.length() > 12) {
            SpannableString snippetText = new SpannableString(snippet);
            snippetText.setSpan(new ForegroundColorSpan(Color.MAGENTA), 0, 10, 0);
            snippetText.setSpan(new ForegroundColorSpan(Color.BLUE), 12, snippet.length(), 0);
            snippetUi.setText(snippetText);
        } else {
            snippetUi.setText("");
        }
    }

    //Sprawdzamy czy kliknięto InfoWindow
    @Override
    public void onInfoWindowClick(Marker marker) {
        System.out.println("Kliknąłeś Info Window");
    }

    @Override
    public void onTaskDone(Object... values) {
        if (currentPolyline != null)
            currentPolyline.remove();
        currentPolyline = mMap.addPolyline((PolylineOptions) values[0]);
    }

    private void updateMarkerWithCircle(LatLng position, String result, double latitude, double longitude) {
        circle.setCenter(position);
        markerCurrentPosition = mMap.addMarker(new MarkerOptions().position(position).title(result));
        Log.d("Current position", "lat = " + latitude + " lon = " + longitude);
        if (isRouted) {
            drawRouteToMarker(currentlyClickedMarker);
            checkInsideOutside();
        }
    }

    private void drawRouteToMarker(Marker clickedMarker) {
        String url = getUrl(markerCurrentPosition.getPosition(), clickedMarker.getPosition(), "walking");
        new FetchURL(MapActivity.this).execute(url, "walking");
    }

    private void checkInsideOutside() {

        float[] distance = new float[2];

        Location.distanceBetween(currentlyClickedMarker.getPosition().latitude, currentlyClickedMarker.getPosition().longitude,
                circle.getCenter().latitude, circle.getCenter().longitude, distance);

        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            if (distance[0] <= 50) {          // mniej niż 50 metrów od celu

                Toast.makeText(getBaseContext(), "Jesteś prawie u celu! (50 metrów) ", Toast.LENGTH_LONG).show();
                //v.vibrate(VibrationEffect.createOneShot(1500, VibrationEffect.DEFAULT_AMPLITUDE));
                startTheGame.setVisibility(View.VISIBLE);

            } else if (distance[0] <= 80) {          // mniej niż 80 metrów od celu

                Toast.makeText(getBaseContext(), "Już niedaleko! (80 metrów) ", Toast.LENGTH_LONG).show();
                //v.vibrate(VibrationEffect.createOneShot(1000, VibrationEffect.DEFAULT_AMPLITUDE));

            } else if (distance[0] <= circle.getRadius()) {        // na granicy koła
                Toast.makeText(getBaseContext(), "Jesteś w pobliżu! (100 metrów) ", Toast.LENGTH_LONG).show();
                //v.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE));

            }

        } else {
            //deprecated in API 26
            //v.vibrate(500);
        }

    }

    private void setMarkers() {

        for(Scenario.Objective objective : scenario.getObjectives()){

            LatLng coordinates = new LatLng(objective.getCoordinates().getLat(),
                    objective.getCoordinates().getLon());

            String placeName = objective.getPlaceName();

            MarkerOptions options =  new MarkerOptions()
                    .position(coordinates)
                    .title(placeName)
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));

            Marker marker = mMap.addMarker(options);

            markersList.add(marker);

        }
    }
}
