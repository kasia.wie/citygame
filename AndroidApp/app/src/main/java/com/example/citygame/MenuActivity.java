package com.example.citygame;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        Button presentationButton = findViewById(R.id.presentationButton);
        Button showProfileButton = findViewById(R.id.showProfileButton);
        Button chooseGameButton = findViewById(R.id.chooseGameButton);
        Button groupButton = findViewById(R.id.groupButton);

        presentationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openPresentationActivity();
            }
        });

        showProfileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openProfileActivity();
            }
        });

        chooseGameButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openChooseGameActivity();
            }
        });

        groupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //         openGroupActivity();              brak aktywnosci
            }
        });


    }

    public void openPresentationActivity() {
        Intent presentation = new Intent(this, PresentationActivity.class);
        startActivity(presentation);
    }

    public void openProfileActivity() {
        Intent profile = new Intent(this, ProfileActivity.class);
        startActivity(profile);
    }

    public void openChooseGameActivity() {
        Intent chooseGame = new Intent(this, ChooseGameActivity.class);
        startActivity(chooseGame);
    }

/*    public void openGroupActivity() {
        Intent group = new Intent(this, GroupActivity.class);
        startActivity(group);
    }
*/

}





