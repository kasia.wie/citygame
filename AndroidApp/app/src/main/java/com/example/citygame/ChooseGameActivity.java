package com.example.citygame;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class ChooseGameActivity extends AppCompatActivity {


    class NetTask extends AsyncTask<String, Integer, String> {

        protected String doInBackground(String... params) {
            scenarios = rester.getNameAndIdScenarios();
            return null;
        }

        protected void onPostExecute(String result) {
            // Stop your progress bar...
            setProuctsListView();
        }
    }


    private List<NameAndIdScenario> scenarios = new ArrayList<>();
    private RESTHandler rester = new RESTHandler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_game);
        Button game1 = findViewById(R.id.game1);

        game1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //openMapActivity();
                getScenariosNameIDs();
            }
        });
    }

    public void setProuctsListView(){

        ListView listView = (ListView)findViewById(R.id.ListView);

        final ArrayList<String> arrayList= new ArrayList<>();
        for (NameAndIdScenario scenario : scenarios) {
            arrayList.add(scenario.toString());
        }

        ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1,
                arrayList);

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                NameAndIdScenario clickedScenario = scenarios.get(position);
                openSingleScenarioView(clickedScenario.getId());
            }
        });
    }

    public void openSingleScenarioView(String id){
        Intent intent = new Intent(getBaseContext(), GameInfoActivity.class);
        intent.putExtra("ID", id);
        startActivity(intent);
    }

    public void openMapActivity() {
        Intent map = new Intent(this, MapActivity.class);
        startActivity(map);
    }

    public void getScenariosNameIDs(){
        NetTask task = new NetTask();
        task.execute();

    }
}