package com.citygame.api;

import com.citygame.models.photoModel.Photo;
import com.citygame.models.scenarioModel.NameAndIdScenario;
import com.citygame.models.scenarioModel.LocalisationScenario;

import com.citygame.service.PrinterService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/scenario")
public class ScenarioApi {

    private PrinterService printer;

    ScenarioApi(PrinterService printer){
        this.printer = printer;
    }


    @PostMapping
    Mono<ResponseEntity<LocalisationScenario>> saveScenario(@RequestBody LocalisationScenario localisationScenario){
       return printer.saveModel(localisationScenario)
               .map(ResponseEntity::ok);
    }

     @GetMapping("/{id}")
    private Mono<ResponseEntity<LocalisationScenario>> getScenario(@PathVariable String id) {
        return printer.findScenarioById(id)
                .map(ResponseEntity::ok);
    }

    @GetMapping("/onlyNameAndId")
    private Flux<NameAndIdScenario> getNameAndId() {
        return printer.getAll();
    }

    @DeleteMapping("/{id}")
    private Mono<Void> deleteScenarioById(@PathVariable String id){
        return printer.deleteById(id);
    }

}



