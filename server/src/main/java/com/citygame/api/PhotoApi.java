package com.citygame.api;

import com.citygame.models.photoModel.DecodedPhotoAndDescription;
import com.citygame.models.photoModel.Photo;
import com.citygame.service.LoaderService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/photo")
@RequiredArgsConstructor
public class PhotoApi {

    private final LoaderService loader;

    @PostMapping
    private Mono<Photo> savePhoto(@RequestBody Photo photo) {
        return loader.savePhoto(photo);
    }

   /* @GetMapping("/{id}")
    private ResponseEntity<Mono<byte[]>> getPhoto(@PathVariable String id) {
        return ResponseEntity.ok()
                //.contentType(MediaType.IMAGE_JPEG)
                .body(loader.getPhoto(id));
    }*/

    @GetMapping("/AllDescriptionsAndPhotos")
    private Flux<Photo> getAllDescriptionsAndPhotos() {
        return loader.getAll();
    }

    @GetMapping("/{id}")
    private Mono<DecodedPhotoAndDescription> findDecodedPhotoAndDescription(@PathVariable String id) {
        return loader.findDecodedPhotoAndDescription(id);
    }
}

