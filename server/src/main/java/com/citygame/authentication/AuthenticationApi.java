package com.citygame.authentication;

import com.citygame.authentication.AuthModels.AuthResponse;
import com.citygame.authentication.AuthModels.User;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

/**
 *
 * @author ard333
 */
@RestController
@RequiredArgsConstructor
public class AuthenticationApi {

    private final JWTUtil jwtUtil;

    private final Encoder passwordEncoder;

    private final UserService userService;


    @PostMapping("/login")
    private Mono<ResponseEntity<?>> login(@RequestBody User user) {
        return userService.findByUsername(user.getUsername()).map(userDetails -> {
            if (passwordEncoder.encode(user.getPassword()).equals(userDetails.getPassword())) {
                return ResponseEntity.ok(new AuthResponse(jwtUtil.generateToken(userDetails)));
            } else {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
            }
        }).defaultIfEmpty(ResponseEntity.status(HttpStatus.UNAUTHORIZED).build());
    }


}