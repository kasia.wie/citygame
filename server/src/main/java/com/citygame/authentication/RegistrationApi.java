package com.citygame.authentication;

import com.citygame.authentication.AuthModels.User;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/registration")
@RequiredArgsConstructor
public class RegistrationApi {

    private final UserService userService;

    @PostMapping
    Mono<ResponseEntity<User>> register(@RequestBody User user){
        return userService.registerUser(user).map(ResponseEntity::ok);
    }

}
