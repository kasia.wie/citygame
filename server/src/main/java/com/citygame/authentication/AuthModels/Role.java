package com.citygame.authentication.AuthModels;
/**
 *
 * @author ard333
 */
public enum Role {
    ROLE_USER, ROLE_ADMIN
}