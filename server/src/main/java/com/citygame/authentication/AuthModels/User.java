package com.citygame.authentication.AuthModels;

import lombok.*;

import java.util.List;
/**
 *
 * @author ard333
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {
    private String username;
    private String password;

    private List<Role> roles = List.of(Role.ROLE_USER);
}
