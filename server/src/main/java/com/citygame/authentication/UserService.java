package com.citygame.authentication;

import com.citygame.authentication.AuthModels.User;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;

    private final Encoder encoder;

    Mono<User> findByUsername(String username) { return userRepository.findByUsername(username); }
    Mono<User> registerUser(User user){

        var pass = user.getPassword();
        user.setPassword(encoder.encode(pass));
        return userRepository.
                save(user);
    }


}