package com.citygame.repository;

import com.citygame.models.photoModel.Base64Photo;
import com.citygame.models.photoModel.Photo;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface PhotoRepository extends ReactiveCrudRepository<Photo, String> {

    Mono<Base64Photo> getPhotoById(String id);
    Flux<Photo> findAllBy();

}
