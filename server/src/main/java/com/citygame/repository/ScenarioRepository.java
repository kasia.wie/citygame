package com.citygame.repository;

import com.citygame.models.scenarioModel.LocalisationScenario;
import com.citygame.models.scenarioModel.NameAndIdScenario;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

import reactor.core.publisher.Flux;

public interface ScenarioRepository extends ReactiveCrudRepository<LocalisationScenario, String> {
    Flux<NameAndIdScenario> findAllBy();
}
