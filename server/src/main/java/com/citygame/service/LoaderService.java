package com.citygame.service;

import com.citygame.models.photoModel.DecodedPhotoAndDescription;
import com.citygame.models.photoModel.Photo;
import com.citygame.repository.PhotoRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Base64;

@Component
@RequiredArgsConstructor
public class LoaderService {

    private final PhotoRepository photoRepository;

    public Mono<Photo> savePhoto(Photo photo) {
        return photoRepository.save(photo);
    }

    public Mono<byte[]> getPhoto(String id) {

        return photoRepository.findById(id)
                .map(Photo::getPhoto)
                //.map(p -> decodePhotoFromBase64(p));
                .map(this::decodePhotoFromBase64);
    }

    public Flux<Photo> getAll() {return photoRepository.findAllBy();
    }

    public Mono<DecodedPhotoAndDescription> findDecodedPhotoAndDescription(String id) {
        return photoRepository.findById(id)
                .map(photo -> new DecodedPhotoAndDescription(photo.getDescription(), decodePhotoFromBase64(photo.getPhoto())));
    }

    private byte[] decodePhotoFromBase64(String photoInBase64){
        return Base64.getDecoder().decode(photoInBase64);
    }




}
