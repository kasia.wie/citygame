package com.citygame.service;

import com.citygame.models.scenarioModel.LocalisationScenario;
import com.citygame.models.scenarioModel.NameAndIdScenario;
import com.citygame.repository.ScenarioRepository;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Component
public class PrinterService {

    private final ScenarioRepository repository;

    public PrinterService(ScenarioRepository repository) {
        this.repository = repository;
    }

    public Mono<LocalisationScenario> saveModel(LocalisationScenario localisationScenario) {
        return repository.save(localisationScenario);
    }

    public Mono<LocalisationScenario> findScenarioById(String id) {
        return repository.findById(id);
    }

    public Flux<NameAndIdScenario> getAll(){return repository.findAllBy();    }

    public Mono<Void> deleteById(String id) {
        return repository.deleteById(id);
    }

}