package com.citygame.models.photoModel;

import lombok.Data;

@Data
public class Photo {
    private String description;
    private String photo;
    private String id;
}
