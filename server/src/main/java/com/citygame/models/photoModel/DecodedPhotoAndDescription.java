package com.citygame.models.photoModel;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class DecodedPhotoAndDescription {

    private String description;
    private byte[] decodedPhoto;
}
