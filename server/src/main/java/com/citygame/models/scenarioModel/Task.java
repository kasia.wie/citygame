package com.citygame.models.scenarioModel;

import lombok.Data;
import java.util.List;

@Data
public class Task {
    private String question;
    private List<PossibleAnswers> possibleAnswers;
}
