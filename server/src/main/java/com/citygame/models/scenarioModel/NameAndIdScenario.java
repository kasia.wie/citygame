package com.citygame.models.scenarioModel;

import lombok.Data;

@Data
public class NameAndIdScenario {
    private String scenarioName;
    private String id;
}
