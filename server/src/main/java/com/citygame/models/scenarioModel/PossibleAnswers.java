package com.citygame.models.scenarioModel;

import lombok.Data;

@Data
public class PossibleAnswers {
    private String answer;
    private Boolean isCorrect ;

}
