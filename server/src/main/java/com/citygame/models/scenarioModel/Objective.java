package com.citygame.models.scenarioModel;

import lombok.Data;
import java.util.List;

@Data
public class Objective {
    // @JsonProperty("place_name")
    private String placeName;
    private Coordinates coordinates;
    private List<Task> tasks;

}
