package com.citygame.models.scenarioModel;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data           // plugin lombok zastepuje adnotacje @Getter @Setter
@Document       //Ta adnotacja oznacza klasę jako obiekt domeny, który chcemy zachować w bazie danych
public class LocalisationScenario {

    private List<Objective> objectives;
    private String scenarioName;
    private String id = null;   //samo bedzie sie uzupelniac jak damy null
}
