package com.citygame.models.scenarioModel;

import lombok.Data;

@Data
public class Coordinates {
    private double lat;
    private double lon;
}
