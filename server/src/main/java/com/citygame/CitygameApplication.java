package com.citygame;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CitygameApplication {

	public static void main(String[] args) {
		SpringApplication.run(CitygameApplication.class, args);
	}

}
